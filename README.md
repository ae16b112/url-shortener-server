Url Shortener

1. Clone the project on your system
https://gitlab.com/ae16b112/url-shortener-server.git

https://gitlab.com/ae16b112/url-shortener-client.git

To run server

1. Go inside in the project directory.
cd server

2. Create virtual environment
virtualenv env

3. Activate environment
source env/bin/activate

4. install dependencies
pip install -r requirements.txt

5. Configure the database based on your local database credentials in settings.py
python manage.py migrate

6. Run server
python manage.py runserver


To run client

1. Go inside in the project directory.
cd url-shortener-client

2. install dependencies
npm install

3. Run client
npm start

Using docker-compose
sudo docker-compose up --build

Features

Shortens your long url


Endpoints

Client-home - http://127.0.0.1:3000/home/

Client-result - http://127.0.0.1:3000/index

server-dashboard - http://127.0.0.1:8000/admin

preview images:

https://drive.google.com/file/d/1j7ab4uiGEbSnJqGiimW5aOA1JELvqg9l/view?usp=sharing

https://drive.google.com/file/d/1hOSxWnFuHxtIWS6qZWIbkKta7Nsj68FS/view?usp=sharing

