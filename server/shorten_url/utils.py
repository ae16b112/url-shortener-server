from urllib.request import HTTPError
from urllib.request import Request
from urllib.request import urlopen


def validate_url(url):
    # Make sure the url is not too short or long
    if len(url) < 4 or len(url) > 500:
        return False
    # If the url contains spaces or does not have any dots
    if (" " in url) or not("." in url):
        return False
    # If the url starts with a dot after http:// or after https:// or just starts with a dot
    if url.lower().startswith("http://.") or url.lower().startswith("https://.") or url.startswith("."):
        return False
    # If the url starts with a slash after http:// or after https:// or just starts with a slash
    if url.lower().startswith("http:///") or url.lower().startswith("https:///") or url.startswith("/"):
        return False
    # If the URL does not start with http:// and https://
    if not(url.lower().startswith("http://")) and not(url.lower().startswith("https://")):
        url = "http://" + url
        return url
    return url


def url_checker(long_url):
    # This function checks URL whether it's a valid URL or not. It sends a
    # request to the URL with a defined Header. If the URL response code is
    # below 400, something like 200, 301 etc then it's valid. Otherwise, the
    # URL is said to be dead.

    try:
        request = Request(long_url, None, {
                          'User-agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5'})
        code = urlopen(request).code
        if (code < 400):
            return True
        else:
            return False
    except HTTPError:
        # handling HTTPError here. If HTTPError is raised, then the URL
        # is dead.
        return False
    
  
  
