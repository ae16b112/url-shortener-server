from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from .models import Url
import uuid
from .utils import validate_url, url_checker
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt

def redirect_short_url(request, pk):
    url_details = Url.objects.get(short_url=pk)
    return redirect(url_details.long_url)

@csrf_exempt
@api_view(['POST'])
def get_shorten_url(request):
    try:
        if request.query_params.get("url"):
            url_details = Url.objects.filter(long_url=request.query_params.get("url")).first()
            return Response({'uid': url_details.short_url}, status=status.HTTP_200_OK)
    except Exception as exception:
        return Response({'error': str(exception)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@csrf_exempt
@api_view(['POST'])
def shorten_url(request):
    try:
        if request.data.get("link"):
            link = request.data.get("link")
            if validate_url(link) and url_checker(link):
                link=validate_url(link)
                if not Url.objects.filter(long_url=link).exists():
                    uid = str(uuid.uuid4())[:10]
                    new_url = Url(long_url=link, short_url =uid)
                    new_url.save()
                else:
                    uid=(Url.objects.filter(long_url=link).first()).short_url
                return Response({'uid': uid}, status=status.HTTP_200_OK)
    except Exception as exception:
        return Response({'error': str(exception)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

