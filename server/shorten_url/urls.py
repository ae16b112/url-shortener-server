from django.urls import path
from . views import redirect_short_url, shorten_url, get_shorten_url

urlpatterns = [
    path('short/<str:pk>', redirect_short_url, name='redirect_short_url'),
    path('shorten_url', shorten_url),
    path('get_shorten_url/', get_shorten_url)
]