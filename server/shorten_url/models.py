from django.db import models

# Create your models here.
class Url(models.Model):
    long_url = models.CharField(max_length=500)
    short_url = models.CharField(max_length=25, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        db_table = "Urls"
        verbose_name = "Url"
        verbose_name_plural = "Urls"
        
    def __str__(self) -> str:
        return self.long_url